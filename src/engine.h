#ifndef ENGINE_H
#define ENGINE_H
#include "globalfunctions.h"
#include "npc.h"
#include "player.h"
#include "event.h"


class Engine {
public:
    Room[] rooms;
    Npc[] npcs;
    Event event;
    Player player;

    void play() {
        // *TODO* Mathew: game loop
    }

    void printManual() {
        // *TODO* Mathew: Prints combat howto
    }

    void printOptions() {
        // *TODO* Mathew: relevant when you have game settings.
    }

    void updateRoom(Room* room, Player* player, Npc* npc) {
        // *TODO* Mathew: places player and enemy in appropriate locations.
    }


};

#endif
