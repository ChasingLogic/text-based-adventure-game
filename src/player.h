
// *TODO* Mathew: Need to add a way to track last known player position for movement. Same for NPC"
class Player {
public:
    int Strength;
    int Endurance;
    int Agility;
    int Wit;
    int Charisma;
    int MissileDef;
    int MagicDef;
    int MeleeDef;
    int Level;
    int Health;
    vector<Loot> inventory;
    vector<Loot> equipped;
    String name;

    void takeDamage(int damage){
        this->Health = this->Health - damage;
    }

    void createCharacter(){
        // *TODO* Mathew: Flesh this out. Needs to have a few preset options.
    }

    void levelUp(){
        // *TODO* Mathew: Worry about this last.
    }

    void updateStats(){
        // *TODO* Mathew: Loop through equipped items and increase stats accordingly.
    }

    void equipGear(){
        // *TODO* Mathew: Adds items to equipped array.
    }

};
