

class Event {
    public:

        void castSpell(String combo, Player* player, Npc* target){
            switch(combo){
                case "Forsa" or "For":
                    spell("ForceBolt", target, player->Wit);

                case "Aer" or "Ae":
                    spell("Wind Wall", target, player->Wit);

                case "Doitean" or "Doi":
                    spell("Fireball", target, player->Wit);

                case "Uisce" or "Uis":
                    spell("Blur", target, player->Wit);

                case "Bas" or "Ba":
                    spell("Curse")

                case "AeDoiFor":
                    spell("Fire Torrent", target, player->Wit);

                case "AeFor":
                    spell("Wind Blast", target, player->Wit);

                case "AeBaFor":
                    spell("Black Plague", target, player->Wit);

                case "UisBaDoi":
                    spell("Black Fireball", target, player->Wit);

                case default:
                    print("Your spell fizzles.");
            }
        }
        
        void NpcDeath(){
           // *TODO* Mathew: Just needs to be some print statements.  
           // *TODO* Mathew: On second thought, could remove the npc from enemy
           // array, perhaps have to change it to a vector for this.
        }

        void Victory() {
          // *TODO* Mathew: More print statements with a way to restart the game.
        }

        void conversation(Player* player, Npc* npc){
            // *TODO* Mathew: Fetches the npc's conversation topics. and
            // handles user input
        }

        void spell(string spellname, Npc* target, int wit){
            // *TODO* Mathew: Self explanatory how this should go.
        }

        void shredArmor(Npc* target1, Player* target2){
            // *TODO* Mathew: Need to look into this more. Possible that
            // it is incorrect to do it this way. Currently though
            // it just will pass nil to whichever isn't the target i.e.
            // if a player is shredding an npc shredArmor(Npc* npc, nil)
        }

        void melee(Npc* target){
            // *TODO* Mathew: Write it.
        }

        // void parry() {
        // *TODO* Mathew: figure this out later get something working first.
        // *TODO* Mathew: Same for all other complex combat actions.
        // }


};
