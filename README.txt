=========== - TODO List- ==========

 - Design Combat System
 - Flesh Out Spell System 
    (Specifically need some functions to handle spell damage.)
 - Figure out how to handle classes (As in Knight, Wizard, etc.)
  - Options include
   - Create a C++ class for each as a subclass of Player
   - Create a check in Player class to decide the effectiveness of a given action based on chosen class
 - Room, Event, Engine, Enemy, and main.cpp haven't even been touched.

===================================

=========== - IDEAS - ============

1. heatlh = 2 + Endurance + 1/2 of Strength

Handle Rooms as multidimensional Arrays that print out sort of like so:

    [-] [-] [-] [-] 
    [-] [-] [*] [-]
    [#] [-] [-] [-]

    key:
        # = Player
        * = NPC

The player can spend one their move action to move x amount of squares so example 1n2e would put the player in front of the NPC.
Should add the printing to engine and tracking/updating in Room class.

==================================

=========== - CLASSES (".h")- ============

Player
 ATTRIBUTES:
 - Strength, Endurance, Agility, Wit, Charisma
 - MissileDef, MagicDef, MeleeDef,
 - Level, Health, Name
 - Spellbook (An array of known spells. Probably better as a map.)
 - Inventory (Vector)
 - Equipped (Vector)
 METHODS:
 - levelUp(Worry about this last or later at the least)
 

----------------------------------------------

NPC
 ATTRIBUTES:
 - Strength, Endurance, Agility, Wit
 - MissileDef, MagicDef, MeleeDef,
 - Level, Health 
 METHODS:
 - takeDamage
 - cast
 - melee
 - talk

----------------------------------------------

Engine
 ATTRIBUTES:
 - RoomsArray
 - EnemiesArray
 METHODS:
 - play (This will be the game loop.)
 - printManual (For a combat system this complex you need a manual.)
 - printOptions
 - drawRoom
 - updateRoom (This could perhaps go into Room.h)

----------------------------------------------

Event
 METHODS:
 - NpcDeath
 - Victory
 - GameOver
 - Battle(Player* player, NPC* enemy)
 - Spell
 - (Possibly more, will have to write as needed.)
 - Conversation
 - equipGear
 - takeDamage
 - castSpell
 - shredArmor
 - meleeAttack
 - parry/riposte
 - study
 - kneeCap
 - shieldUp
 - shieldBash
 - kick
----------------------------------------------

Room
 - (Not sure if room should be a class or multiple small classes)
 ATTRIBUTES
 - length, width
 - space[length][width]
 METHODS
 - populateRoom
 - fillWithLoot
 - getEvent
----------------------------------------------

Loot
 ATTRIBUTES
 - StrMod, EndMod, AgMod, WitMod
 - MissDefMod, MagDefMod, MelDefMod
==============================================
