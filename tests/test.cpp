#include <iostream>
#include <vector>
using namespace std;

class Room {
public:
    Room(int l, int w){
        length = l;
        width = w;
        space.resize(length);
        for(int i = 0; i < space.size(); ++i){
            space[i].resize(width);
        }
    }
    void printRoom(){
        for(int row = 0; row < this->space[0].size() ; ++row){
            for(int col = 0; col < this->space.size(); ++col){
                if(col + 1 == this->space.size()){
                    cout << this->space[col][row] << endl;
                } else {
                    cout << this->space[col][row];
                }
            }
        }
    }

    void populateRoom(){
        for(int col = 0; col < this->space.size(); ++col){
            for(int row = 0; row < this->space[0].size(); ++row){
                // This checks if you are on the edge of the room.
                if((col == 0 || col + 1 == this->space.size()) && (row + 1 == this->space[0].size() || row == 0)){
                    // This will check if you're on a left corner.
                    if(col == 0 && (row == 0 || row + 1 == this->space[0].size())){
                        this->space[col][row] = '[';
                    // This will check if you're on a right corner.
                    } else if(col + 1 == this->space.size() && (row + 1 == this->space[0].size() || row == 0)) {
                        this->space[col][row] = ']';
                    } 
                } else {
                    // So if your not on a corner AND your on an edge of a room 
                    // AND it's not the top or bottom it will print a |
                    if (col == 0 || col + 1 == this->space.size()){
                        this->space[col][row] = '|';
                    // Now if you're not on a corner AND you're on an edge BUT
                    // you're not on the sides it will print a -
                    } else if (row == 0 || row + 1 == this->space[0].size()){
                        this->space[col][row] = '-';
                    // Else print a blank square.
                    } else {
                    this->space[col][row] = ' ';
                    }
                }
                
            }
        }
    }
    
private:
    int length;
    int width;
    vector< vector<char> > space;
};

int main() {
    Room room(3, 5);

    room.populateRoom();

    room.printRoom();


    return 0;
}
